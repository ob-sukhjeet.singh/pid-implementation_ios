//
//  StringExtension.swift
//  PID Implementation_iOS
//
//  Created by Sejal Khanna on 02/03/22.
//

import Foundation

extension String {
    
    func formattedDateFromString(dateString: String, inputFormat: String? = nil, withFormat format: String, locale: String? = nil) -> String? {
        let inputFormatter = DateFormatter()
        //  2021-09-25 06:43:42 +0000
        inputFormatter.dateFormat = inputFormat ?? "yyyy-MM-dd HH:mm:ss z"
        inputFormatter.timeZone = TimeZone(identifier: "GMT")
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            outputFormatter.locale = Locale(identifier: "en")
            outputFormatter.timeZone = TimeZone(identifier: locale ?? "GMT")
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
}
