//
//  ViewController.swift
//  PID Implementation_iOS
//
//  Created by Sejal Khanna on 02/03/22.
//

import UIKit
import CommonCrypto
import CryptoKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var pidLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    func initialize(){
        
        let timestamp = String().formattedDateFromString(dateString: "\(Date())", withFormat: "YYYY-MM-dd'T'hh:mm:ss", locale: "IST")!
        // doubt
        let wadh = Data(SHA256.hash(data: createWadh(with: timestamp).data(using: .utf8)!)).base64EncodedString()
        
        let StringToEncrypt = String(format: "<Pid ts='\(timestamp)' ver='2.0' wadh='\(wadh)'><Pv otp='' pin=''/></Pid>")
        print(StringToEncrypt)
        
        let messageData = StringToEncrypt.data(using: .utf8)!
        // key data ?
        // ivData ?
        let keyData = "1234567890123456".data(using:String.Encoding.utf8)!
        let ivData = getBytes(for: timestamp.data(using: .utf8)!, number: 12)
        let tag = getBytes(for: timestamp.data(using: .utf8)!, number: 16)
        
        do {
            let encryptedData = try AES.GCM.seal(messageData, using: SymmetricKey(data: keyData), nonce: AES.GCM.Nonce(data: ivData), authenticating: tag)
            print(encryptedData.ciphertext.base64EncodedString())
        } catch {
            print(error)
        }
    }
    
    func getBytes(for data: Data , number: Int) -> Data {
        
        let bytes: [UInt8] = Array(data)
        let newData = Data(count: number)
        var newBytes: [UInt8] = Array(data)
        
        let count = data.count
        
        for i in 0..<number {
            newBytes[i] = bytes[count - number + i]
        }
        print(newBytes)
        return Data(bytes: newBytes, count: number)
    }
    
    func createWadh(with time: String) -> String {
        let timeStamp = time
        let version = "2.0"
        let resAuth = "O"
        let rc = "Y"
        let lr = "Y"
        let de = "Y"
        let pfr = "Y"
        
        return "\(timeStamp)\(version)\(timeStamp)\(resAuth)\(rc)\(lr)\(de)\(pfr)"
    }
}

